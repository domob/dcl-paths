/*
    dcl-paths, a Decentraland game about disappearing floors
    Copyright (C) 2019  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Level from "level";

/** Materials for the tiles with certain steps left.  */
const materials: (Material | null)[] = [null];
for (let i: number = 1; i <= 4; ++i)
  {
    const m: Material = new Material ();
    m.metallic = 0;
    m.roughness = 1;
    materials.push (m);
  }
materials[1].albedoColor = Color3.Red ();
materials[2].albedoColor = Color3.Yellow ();
materials[3].albedoColor = Color3.Blue ();
materials[4].albedoColor = Color3.Green ();

/**
 * Sizes (relative to the total size of one) for tiles of a certain number
 * of steps remaining.
 */
const sizes: (number | null)[] =
  [
    null,
    0.2,
    0.5,
    0.8,
    1,
  ];

/** Height of the tile cylinders.  */
const tileHeight: number = 0.02;

/** Transforms for the tiles of each size.  */
const tileTransforms: (Transform | null)[] = [null];
for (let i: number = 1; i <= 4; ++i)
  {
    const t: Transform = new Transform ();
    t.position.set (0.5, -1, 0.5);
    t.scale.set (sizes[i], 1, sizes[i]);
    tileTransforms.push (t);
  }

/** Basic cylinder used for the (visible) tiles.  */
const tileCylinder: CylinderShape = new CylinderShape ();
tileCylinder.radiusTop = 0.5;
tileCylinder.radiusBottom = 0.5;

/** Invisible plane used as floor collider on the tiles.  */
const floorPlane: GLTFShape = new GLTFShape ("models/oneway/model.gltf");

/** Basic transform for the floor plane (not including positioning).  */
const floorTransform: Transform = new Transform ();
floorTransform.rotation.setEuler (90, 0, 0);
floorTransform.position.set (1, 0, 0);
floorTransform.scale.set (1, 1, -1);

/**
 * An invisible barrier between the game board and the finish area.
 * It can be enabled and disabled, depending on whether the level
 * has been solved.
 */
class FinishBarrier extends Entity
{

  parent: Entity;
  active: boolean;

  constructor (p: Entity, numTiles: number)
  {
    super ();

    this.addComponent (floorPlane);

    const transf: Transform = new Transform ();
    transf.scale.set (numTiles, 1, -1);
    transf.position.set (numTiles, 0, numTiles);
    this.addComponent (transf);

    this.parent = p;
    this.setActive (true);
  }

  setActive (val: boolean): void
  {
    if (this.active == val)
      return;

    this.active = val;
    if (this.active)
      this.setParent (this.parent);
    else
      {
        this.setParent (null);
        engine.removeEntity (this);
      }
  }

}

/**
 * A single tile of the board.
 */
class Tile extends Entity
{

  stepsLeft: number;

  board: Entity;
  shape: Entity;

  inWorld: boolean;

  constructor (b: Entity, x: number, y: number)
  {
    super ();

    this.board = b;

    const tileTransform: Transform = new Transform ();
    tileTransform.position.set (x, 0, y);
    tileTransform.scale.set (1, tileHeight, 1);
    this.addComponent (tileTransform);

    this.shape = new Entity ();
    this.shape.addComponent (tileCylinder);
    this.shape.setParent (this);

    const floor: Entity = new Entity ();
    floor.addComponent (floorPlane);
    floor.addComponent (floorTransform);
    floor.setParent (this);

    this.stepsLeft = 0;
    this.inWorld = false;
  }

  getLeft (): number
  {
    return this.stepsLeft;
  }

  setLeft (num: number): void
  {
    this.stepsLeft = num;
    if (this.stepsLeft > 0)
      {
        this.shape.addComponentOrReplace (materials[this.stepsLeft]);
        this.shape.addComponentOrReplace (tileTransforms[this.stepsLeft]);
        if (!this.inWorld)
          {
            this.inWorld = true;
            this.setParent (this.board);
          }
      }
    else if (this.inWorld)
      {
        this.setParent (null);
        engine.removeEntity (this);
        this.inWorld = false;
      }
  }

}

/**
 * Visualisation of the game board.  This simply holds the current configuration
 * of tiles (including their remaining steps) and updates the entities
 * accordingly.  It does not do any logic by itself.
 *
 * The board is square, with a given number of tiles.  It is placed at height
 * zero, and from (0, 0) to the first quadrant of the plane, with tiles
 * sized at one.  Users should then apply a transform as needed to fit that
 * into the game scene.
 */
export default class Board extends Entity
{

  numTiles: number;
  totalSteps: number;

  tiles: Tile[][];
  barrier: FinishBarrier;

  constructor (num: number)
  {
    super ();
    this.numTiles = num;

    this.tiles = [];
    for (let x: number = 0; x < this.numTiles; ++x)
      {
        this.tiles.push ([]);
        for (let y: number = 0; y < this.numTiles; ++y)
          this.tiles[x].push (new Tile (this, x, y));
      }
    this.totalSteps = 0;

    this.barrier = new FinishBarrier (this, this.numTiles);
    this.barrier.setActive (true);
  }

  /**
   * Sets the current configuration based on the level.
   */
  setLevel (lvl: Level): void
  {
    this.totalSteps = 0;
    for (let x: number = 0; x < this.numTiles; ++x)
      for (let y: number = 0; y < this.numTiles; ++y)
        {
          this.tiles[x][y].setLeft (lvl.steps[x][y]);
          this.totalSteps += lvl.steps[x][y];
        }
    this.barrier.setActive (true);
  }

  /**
   * Decrements a tile's steps left, as happens when the player leaves it.
   * Returns false if the step could not be decremented because it was already
   * at zero.
   */
  decrementSteps (x: number, y: number): boolean
  {
    const cur: number = this.tiles[x][y].getLeft ();
    const ok: boolean = (cur > 0);

    if (ok)
      {
        this.tiles[x][y].setLeft (cur - 1);
        --this.totalSteps;
      }

    this.barrier.setActive (this.totalSteps > 1);

    return ok;
  }

}
