/*
    dcl-paths, a Decentraland game about disappearing floors
    Copyright (C) 2019-2020  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Board from "board";
import Level from "level";
import PlayerPosition from "playerpos";

/* ************************************************************************** */

/** Height of the game platform above ground.  */
const height: number = 6;

/** Number of tiles in the game board (side of the square).  */
const numTiles: number = 7;

/** Total width of the scene.  */
const width: number = 16;

/** Total depth of the scene.  */
const depth: number = 32;

/** Size of each tile, as well as the ramp leading up.  */
const tileSize: number = width / numTiles;

/** y coordinate where the ramp starts within the scene.  */
const rampStart: number = 1;

/** y coordinate where the board starts within the scene.  */
const boardStart: number = 10;

/** y coordinate where the after platform ends.  */
const afterEnd: number = depth - 2;

/** Material for the ramp, starting and finish floors.  */
const rampMaterial: Material = new Material ();
rampMaterial.albedoColor = new Color3 (0.4, 0.4, 0.4);
rampMaterial.metallic = 0;
rampMaterial.roughness = 1;

/** Material for the ground floor.  */
const groundMaterial: Material = new Material ();
groundMaterial.albedoColor = new Color3 (0.0, 0.5, 0.0);
groundMaterial.metallic = 0;
groundMaterial.roughness = 1;

/** Series of level difficulty (in terms of max steps in the level).  */
const difficulties: number[] = [1, 2, 4];

/* ************************************************************************** */

/**
 * Builds up the basic "frame" of the game scene.  This is the ramp leading
 * up and the platforms before/after the game board itself.
 */
function SceneFrame (): Entity
{
  const plane: PlaneShape = new PlaneShape ();

  const midX: number = tileSize * numTiles / 2;
  const rampEndY: number = boardStart - tileSize;
  const afterStart: number = boardStart + tileSize * numTiles;

  /* For the ramp, we need to compute its actual "length" and the
     angle we need to rotate it, so that it fits precisely into the
     desired range and closes up to the "before" platform.  */
  const rampAngle: number = 180 / Math.PI * Math.atan2 (rampEndY - rampStart,
                                                        height);
  const rampLength: number = Math.sqrt (Math.pow (rampEndY - rampStart, 2)
                                          + Math.pow (height, 2));

  const platformBefore: Entity = new Entity ();
  platformBefore.addComponent (plane);
  platformBefore.addComponent (rampMaterial);
  const beforeTransform: Transform = new Transform ();
  beforeTransform.scale.setAll (tileSize);
  beforeTransform.rotation.setEuler (90, 0, 0);
  beforeTransform.position.set (midX, height, rampEndY + tileSize / 2);
  platformBefore.addComponent (beforeTransform);

  const platformAfter: Entity = new Entity ();
  platformAfter.addComponent (plane);
  platformAfter.addComponent (rampMaterial);
  const afterTransform: Transform = new Transform ();
  afterTransform.scale.set (tileSize * numTiles, afterEnd - afterStart, 1);
  afterTransform.rotation.setEuler (90, 0, 0);
  afterTransform.position.set (midX, height, (afterStart + afterEnd) / 2);
  platformAfter.addComponent (afterTransform);

  const ramp: Entity = new Entity ();
  ramp.addComponent (plane);
  ramp.addComponent (rampMaterial);
  const rampTransform: Transform = new Transform ();
  rampTransform.scale.set (tileSize, rampLength, 1);
  rampTransform.rotation.setEuler (rampAngle, 0, 0);
  rampTransform.position.set (midX, height / 2, (rampStart + rampEndY) / 2);
  ramp.addComponent (rampTransform);

  const ground: Entity = new Entity ();
  ground.addComponent (plane);
  ground.addComponent (groundMaterial);
  const groundTransform: Transform = new Transform ();
  groundTransform.scale.set (width, depth, 1);
  groundTransform.rotation.setEuler (90, 0, 0);
  groundTransform.position.set (width / 2, 0, depth / 2);
  ground.addComponent (groundTransform);

  const res: Entity = new Entity ();
  platformBefore.setParent (res);
  platformAfter.setParent (res);
  ramp.setParent (res);
  ground.setParent (res);

  return res;
}

/* ************************************************************************** */

/** UI canvas for displaying text messages.  */
const canvas: UICanvas = new UICanvas ();
canvas.visible = false;
canvas.isPointerBlocker = false;

/** Rect to hold the message text.  */
const uiRect: UIContainerRect = new UIContainerRect (canvas);
uiRect.width = "30%";
uiRect.height = "50%";
uiRect.hAlign = "right";
uiRect.vAlign = "bottom";
uiRect.color = Color4.Black ();

/** Text element in the UI canvas.  */
const uiText: UIText = new UIText (uiRect);
uiText.width = "100%";
uiText.height = "100%";
uiText.hTextAlign = "center";
uiText.vTextAlign = "center";
uiText.textWrapping = true;
uiText.color = Color4.White ();
uiText.fontSize = 14;

/**
 * Displays a message in the on-screen UI.  Set the message to null to
 * stop displaying any message instead.
 */
function ShowMessage (msg: string | null): void
{
  if (msg == null)
    {
      canvas.visible = false;
      uiText.value = "";
      return;
    }

  uiText.value = msg;
  canvas.visible = true;
}

/** UI message shown to explain the game.  */
const msgStart: string =
  "Welcome!  Try to reach the platform on the other side.\n\n"
  + "But beware--each step will reduce the stones!\n\n"
  + "You can only enter the finish after visiting each stone the"
  + " correct number of times.\n\n"
  + "If you are careful, you can walk."
  + "  Try to avoid jumps to save your energy.\n\n"
  + "Return to the start platform to try the level again.";

/** UI message shown when the user completes the game.  */
const msgFinish: string =
  "Congratulations!\n\n"
  + "Return to the start platform for another challenge!";

/** UI message shown when the player completed with cheating.  */
const msgCheat: string = "You tricky jumper!";

/* ************************************************************************** */

/**
 * Custom PlayerPosition system that implements the callbacks as needed
 * for our game.
 */
class PlayerHandler extends PlayerPosition
{

  /** The board we update for tile moves.  */
  board: Board;

  /** The level we use with the current level data.  */
  lvl: Level;

  /** The difficulty index for the next level.  */
  nextDifficulty: number;

  /** Whether cheating was detected in the current game.  */
  cheater: boolean;

  constructor (b: Board)
  {
    super (height, numTiles, tileSize, boardStart);
    this.nextDifficulty = 0;
    this.board = b;
    this.lvl = new Level (numTiles);
    this.createNextLevel ();
  }

  /**
   * Resets the level with the next difficulty we want to use.
   */
  createNextLevel (): void
  {
    this.lvl.randomise (difficulties[this.nextDifficulty]);
    if (this.nextDifficulty < difficulties.length - 1)
      ++this.nextDifficulty;
  }

  onTileLeft (x: number, y: number): void
  {
    if (!this.board.decrementSteps (x, y))
      {
        log ("Likely cheating detected");
        this.cheater = true;
      }
  }

  onStart (): void
  {
    this.board.setLevel (this.lvl);
    this.cheater = false;

    ShowMessage (msgStart);
  }

  onStartLeft (): void
  {
    ShowMessage (null);
  }

  onFinish (): void
  {
    if (this.cheater)
      ShowMessage (msgCheat);
    else
      ShowMessage (msgFinish);

    this.createNextLevel ();
  }

  onFinishLeft (): void
  {
    ShowMessage (null);
  }

}

/* ************************************************************************** */

const frame: Entity = SceneFrame ();
engine.addEntity (frame);

const boardWrapper: Entity = new Entity ();
const boardTransform: Transform = new Transform ();
boardTransform.scale.setAll (tileSize);
boardTransform.position.set (0, height, boardStart);
boardWrapper.addComponent (boardTransform);

const board: Board = new Board (numTiles);
board.setParent (boardWrapper);
engine.addEntity (boardWrapper);

const handler: PlayerHandler = new PlayerHandler (board);
engine.addSystem (handler);
