/*
    dcl-paths, a Decentraland game about disappearing floors
    Copyright (C) 2019  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
 * Generates a pseudo-random integer between the two bounds, both inclusive.
 */
function randInt (min: number, max: number): number
{
  return Math.floor ((max - min + 1) * Math.random () + min);
}

/**
 * A coordinate on the board (or a direction to move).
 */
class Coord
{

  x: number;
  y: number;

  constructor (xx: number, yy: number)
  {
    this.x = xx;
    this.y = yy;
  }

  /**
   * Returns true if this coordinate is within the bounds for the given
   * number of tiles.
   */
  isOnBoard (numTiles: number): boolean
  {
    if (this.x < 0 || this.y < 0)
      return false;
    if (this.x >= numTiles || this.y >= numTiles)
      return false;

    return true;
  }

  /**
   * Adds a given point to this one.
   */
  add (pt: Coord): void
  {
    this.x += pt.x;
    this.y += pt.y;
  }

  /**
   * Computes how far we can go in the given direction before we reach
   * the edge of the board.
   */
  stepsToEdge (numTiles: number, dir: Coord): number
  {
    const tester: Coord = new Coord (this.x, this.y);

    let res: number = 0;
    while (true)
      {
        tester.add (dir);
        if (!tester.isOnBoard (numTiles))
          return res;
        ++res;
      }
  }

}

/**
 * A generic representation of a game level, without anything concrete about
 * its visualisation of the logic to play it.
 */
export default class Level
{

  numTiles: number;
  steps: number[][];

  constructor (num: number)
  {
    this.numTiles = num;

    this.steps = [];
    for (let x: number = 0; x < this.numTiles; ++x)
      {
        this.steps.push ([]);
        for (let y: number = 0; y < this.numTiles; ++y)
          this.steps[x].push (0);
      }
  }

  /**
   * Generates a random level.
   */
  randomise (maxSteps: number): void
  {
    for (let x: number = 0; x < this.numTiles; ++x)
      for (let y: number = 0; y < this.numTiles; ++y)
        this.steps[x][y] = 0;

    /* Level generation works by just creating a random path and incrementing
       tiles along it.  This essentially just creates the path the player then
       needs to follow to solve the level.

       We do that by starting at the initial starting position and then
       randomly generating movements in each of the four possible directions
       and with random lengths.

       When we increment a tile to the maximum number of steps for the
       first time, we start moving only towards the finish and into
       one of the two random orthogonal directions (left/right).  This
       ensures that we never have to increment a tile beyond the maximum.  */

    const pos: Coord = new Coord (Math.floor ((this.numTiles - 1) / 2), 0);
    this.incrementSteps (pos);

    let directions: Coord[] =
      [
        new Coord (-1, 0),
        new Coord (1, 0),
        new Coord (0, -1),
        new Coord (0, 1),
      ];
    let finishing: boolean = false;

    let dir: Coord;
    let stepsLeft: number = 0;

    while (true)
      {
        if (stepsLeft == 0)
          {
            const dirInd: number = randInt (0, directions.length - 1);
            dir = directions[dirInd];
            const stepsPossible: number = pos.stepsToEdge (this.numTiles, dir);

            /* Stop if we are on the edge of the finish area, already have seen
               a maximum steps tile, and would go off the board next.  */
            if (stepsPossible == 0 && finishing && pos.y == this.numTiles - 1)
              break;

            /* Otherwise just pick a new direction if we can't go there.  */
            if (stepsPossible == 0)
              continue;

            stepsLeft = randInt (1, stepsPossible);
          }

        --stepsLeft;
        pos.add (dir);

        const reachedMax: boolean = (this.incrementSteps (pos) >= maxSteps);
        if (reachedMax && !finishing)
          {
            finishing = true;
            directions = [new Coord (0, 1)];
            if (randInt (0, 1) == 0)
              directions.push (new Coord (-1, 0));
            else
              directions.push (new Coord (1, 0));

            stepsLeft = 0;
          }
      }
  }

  /**
   * Increments the step number at the given position and returns the new one.
   */
  incrementSteps (c: Coord): number
  {
    return ++this.steps[c.x][c.y];
  }

}
