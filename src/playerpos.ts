/*
    dcl-paths, a Decentraland game about disappearing floors
    Copyright (C) 2019  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const camera: Camera = Camera.instance;

/**
 * Logical representation of the user's position.  This can be "on a tile"
 * (with given x/y indices) or "something else".
 */
class UserPosition
{

  static readonly ELSE = 0;
  static readonly TILE = 1;
  static readonly START = 2;
  static readonly FINISH = 3;

  type: number;
  x: number;
  y: number;

  constructor ()
  {}

  setElse (): void
  {
    this.type = UserPosition.ELSE;
  }

  setTile (x: number, y: number): void
  {
    this.type = UserPosition.TILE;
    this.x = x;
    this.y = y;
  }

  setStart (): void
  {
    this.type = UserPosition.START;
  }

  setFinish (): void
  {
    this.type = UserPosition.FINISH;
  }

  /**
   * Compares for equality to another logical position.
   */
  equals (other: UserPosition): boolean
  {
    if (this.type != other.type)
      return false;

    switch (this.type)
      {
      case UserPosition.TILE:
        return this.x == other.x && this.y == other.y;

      case UserPosition.ELSE:
      case UserPosition.START:
      case UserPosition.FINISH:
        return true;
      }
  }

  toString (): String
  {
    switch (this.type)
      {
      case UserPosition.TILE:
        return "Tile (" + this.x + ", " + this.y + ")";
      case UserPosition.START:
        return "Start";
      case UserPosition.FINISH:
        return "Finish";
      case UserPosition.ELSE:
        return "Else";
      }
  }

}

/**
 * A system that tracks the player's position and notifies callbacks about
 * when the player moves between certain places.  For instance, enters the
 * start platform or moves from one tile to another.
 */
export default class PlayerPosition implements ISystem
{

  height: number;
  numTiles: number;
  tileSize: number;
  boardStart: number;

  /** The previously known user position.  */
  prevPos: UserPosition;

  constructor (h: number, n: number, ts: number, bs: number)
  {
    this.height = h;
    this.numTiles = n;
    this.tileSize = ts;
    this.boardStart = bs;

    this.prevPos = new UserPosition ();
    this.prevPos.setElse ();
  }

  update (): void
  {
    const pos: Vector3 = camera.position;
    const current: UserPosition = this.getUserPosition (pos);

    if (current.equals (this.prevPos))
      return;

    log ("Player changed logical position: " + current);

    if (this.prevPos.type == UserPosition.TILE)
      this.onTileLeft (this.prevPos.x, this.prevPos.y);

    if (this.prevPos.type == UserPosition.START)
      this.onStartLeft ();
    if (current.type == UserPosition.START)
      this.onStart ();

    if (this.prevPos.type == UserPosition.FINISH)
      this.onFinishLeft ();
    if (current.type == UserPosition.FINISH)
      this.onFinish ();

    this.prevPos = current;
  }

  /**
   * This method gets called whenever the user left a tile.  It should be
   * overwritten by a subclass to perform the needed action (e.g. reduce the
   * tile's remaining steps).
   */
  onTileLeft (x: number, y: number): void
  {}

  /**
   * This method gets called whenever the user enters the "start" area.
   */
  onStart (): void
  {}

  /**
   * This method gets called whenever the user exits the "start" area.
   */
  onStartLeft (): void
  {}

  /**
   * This method gets called whenever the user enters the "finish".
   */
  onFinish (): void
  {}

  /**
   * This method gets called whenever the user exits the "finish" area.
   */
  onFinishLeft (): void
  {}

  /**
   * Returns the tile as [x, y] vector of the user, based on the given
   * real-world coordinates.  The tile indices may be out of range.
   */
  getTile (x: number, y: number): number[]
  {
    y -= this.boardStart;
    const tileX: number = Math.floor (x / this.tileSize);
    const tileY: number = Math.floor (y / this.tileSize);

    return [tileX, tileY];
  }

  /**
   * Returns the logical user position based on their real-world coordinates.
   */
  getUserPosition (pos: Vector3): UserPosition
  {
    const res: UserPosition = new UserPosition ();

    /* If the user is below the game level, it is always "something else".  */
    if (pos.y < this.height)
      {
        res.setElse ();
        return res;
      }

    /* If the user is "beyond" the board, he's in finish.  */
    if (pos.z > this.boardStart + this.numTiles * this.tileSize)
      {
        res.setFinish ();
        return res;
      }

    /* Check if the user is on a tile.  */
    const tile: number[] = this.getTile (pos.x, pos.z);
    if (tile[0] >= 0 && tile[0] < this.numTiles
          && tile[1] >= 0 && tile[1] < this.numTiles)
      {
        res.setTile (tile[0], tile[1]);
        return res;
      }

    /* Check for the start platform.  */
    if (tile[0] == (this.numTiles - 1) / 2 && tile[1] == -1)
      {
        res.setStart ();
        return res;
      }

    res.setElse ();
    return res;
  }

}
